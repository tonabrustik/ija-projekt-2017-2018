package app;

import app.gui.Manager;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Antonín Bruštík
 */

public class App extends Application {
    
    @Override
    public void start(Stage stage) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/MainApp.fxml"));
        
        Parent root = (Parent) loader.load();
        Scene scene = new Scene(root);
        
        stage.setTitle("Schematix");
        stage.setScene(scene);
        stage.show();
        
        Manager manager = (Manager) loader.getController();
        manager.setStage(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
