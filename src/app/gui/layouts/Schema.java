package app.gui.layouts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.input.DragEvent;

import app.gui.Manager;
import app.gui.layouts.iterator.BlockIterator;
import components.Block;
import components.Connection;
import components.SchemaObject;
import graphic.GraphicRepresentation;

/**
 *
 * @author Antonín Bruštík
 */

public class Schema
{
    public static int EDIT = 0;
    public static int RUN  = 1;
    
    private final AnchorPane schema = new AnchorPane();
    private final ButtonBar controls = new ButtonBar();
    private final TitledPane result = new TitledPane();
    
    private int schemaMode = EDIT;
    
    private final Set<Block> leafBlocks = new HashSet<>();
    private BlockIterator evalIterator = null;
    
    private final Manager manager;

    
    // ------------------------------------------------------------------------
    
    public Schema(Manager manager)
    {
        this.manager = manager;
        
        Button backward = new Button("Zpět");
        Button forward = new Button("Vpřed");
        Button play = new Button("Spustit");
        Button stop = new Button("Zastavit");
        
        ButtonBar.setButtonData(stop, ButtonData.LEFT);
        
        backward.setOnAction(e -> {this.manager.evalPreviousBlock();});
        forward.setOnAction(e -> {this.manager.evalNextBlock();});
        play.setOnAction(e -> {this.manager.evalAllBlocks();});
        stop.setOnAction(e -> {this.manager.editSchema();});
        
        this.controls.getButtons().addAll(stop, play, forward, backward);
        this.controls.setPadding(new Insets(10, 10, 10, 10));
    }
    
    
    
    // open -------------------------------------------------------------------
    public AnchorPane open()
    {
        return this.schema;
    }
    
    
    
    // run mode ---------------------------------------------------------------
    public void run()
    {
        if (this.isMode(EDIT))
        {
            this.schemaMode = RUN;

            this.schema.getChildren().forEach((n) ->
            {
                ((GraphicRepresentation) n).setSchemaRunMode();
            });

            this.evalIterator = new BlockIterator(this.makeEvalQueue());
            
            TextArea text = new TextArea();
            text.setEditable(false);
            this.result.setContent(text);
            
            this.manager.rightVbox.getChildren().addAll(this.result, this.controls);
        }
    }
    
    
    
    // edit mode --------------------------------------------------------------
    public void edit()
    {
        if (this.isMode(RUN))
        {
            this.schemaMode = EDIT;

            this.schema.getChildren().forEach((n) ->
            {
                ((GraphicRepresentation) n).setSchemaEditMode();
            });

            if (this.evalIterator != null && this.evalIterator.hasActual())
            {
                this.evalIterator.actual().setDefault();
            }

            this.manager.rightVbox.getChildren().removeAll(this.result, this.controls);

            this.evalIterator = null;
        }
    }
    
    
    // run mode print ---------------------------------------------------------
    public void print(String str)
    {
        if (this.isMode(RUN))
        {
            ((TextArea) this.result.getContent()).appendText(str);
        }
    }
    
    
    // schema mode ------------------------------------------------------------
    public boolean isMode(int mode)
    {
        return this.schemaMode == mode;
    }
    
    
    
    // make evaluation queue --------------------------------------------------
    public List<Block> makeEvalQueue()
    {
        List<Block> blocksToProcess = new ArrayList<>(this.leafBlocks);
        List<Block> evalQueue = new ArrayList<>();
        
        while(!blocksToProcess.isEmpty())
        {
            Block block = blocksToProcess.get(0);
            
            blocksToProcess.addAll(1, block.getAncestors());
            blocksToProcess.removeAll(Collections.singleton(block));
            
            if (evalQueue.contains(block))
            {
                evalQueue.remove(block);
            }
            
            evalQueue.add(0, block);
        }
        
        return evalQueue;
    }
    
    
    
    // block queue iteration --------------------------------------------------
    public boolean hasNextEvalBlock()
    {
        return this.evalIterator.hasNext();
    }
    
    public Block getNextEvalBlock()
    {
        if (this.evalIterator.actual() != null)
        {
            this.evalIterator.actual().setDefault();
        }
        
        this.evalIterator.next().setHighlight();
        return this.evalIterator.actual();
    }
    
    public boolean hasPreviousEvalBlock()
    {
        return this.evalIterator.hasPrevious();
    }
    
    public Block getPreviousEvalBlock()
    {
        if (this.evalIterator.actual() != null)
        {
            this.evalIterator.actual().setDefault();
        }
        
        this.evalIterator.previous().setHighlight();
        return this.evalIterator.actual();
    }
    
    public Block getActualEvalBlock()
    {
        return this.evalIterator.actual();
    }
    
    
    
    // adding items -----------------------------------------------------------
    public void insertBlock(Block block)
    {
        schema.getChildren().add(block.getGraphicRepresentation());
    }
    
    public void insertConnection(Connection connection)
    {
        schema.getChildren().add(0, connection.getGraphicRepresentation());
    }
    
    public void insert(int index, SchemaObject object)
    {
        schema.getChildren().add(index, object.getGraphicRepresentation());
    }
    
    public void insert(GraphicRepresentation object)
    {
        schema.getChildren().add(object);
    }
    
    
    
    // removing items ---------------------------------------------------------
    public void remove(SchemaObject object)
    {
        schema.getChildren().remove(object.getGraphicRepresentation());
    }
    
    public void remove(GraphicRepresentation object)
    {
        schema.getChildren().remove(object);
    }
    
    
    
    // evaluation hierarchy ---------------------------------------------------
    public void setBlockAsLeaf(Block block)
    {
        this.leafBlocks.add(block);
    }
    
    public void setBlockAsBranch(Block block)
    {
        this.leafBlocks.remove(block);
    }
    
       
    
    // port drag --------------------------------------------------------------
    public void setDrawConnectionLine(EventHandler<? super DragEvent> eh)
    {
        schema.setOnDragOver(eh);
    }
    
    
    
    // clear ------------------------------------------------------------------
    public void clear()
    {
        this.schema.getChildren().clear();
        this.leafBlocks.clear();
    }
    
}
