package app.gui.layouts;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import app.gui.Manager;
import components.Block;
import components.Port;
import components.math.Equation;

/**
 *
 * @author Antonín Bruštík
 */

public class Editor
{
    private final Manager manager;
    
    
    // ------------------------------------------------------------------------
    

    public Editor(Manager manager)
    {
        this.manager = manager;
    }
    
    
    public BorderPane open(Block block)
    {
        BorderPane editor = new BorderPane();
        
        editor.setLeft(this.makeInputs(block));
        editor.setRight(this.makeOutputs(block));
        editor.setCenter(this.makeEquations(block));
        editor.setTop(this.makeControlButtons(block));
        editor.setBottom(this.makeBlockName(block));
                
        return editor;
    }
    
    
    private VBox makeInputs(Block block)
    {
        Text inVboxTitle = new Text("Vstupy");
        inVboxTitle.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        
        VBox inVbox = new VBox(inVboxTitle);
        inVbox.setPadding(new Insets(10));
        inVbox.setSpacing(8);
        inVbox.setMinWidth(300);
        inVbox.setAlignment(Pos.CENTER);
        
        block.getInputs().forEach((input) -> {
            inVbox.getChildren().add(addPortField(input));
        });
        
        Button inVboxAddButton = new Button("Přidat vstup");
        inVboxAddButton.setOnAction(e ->
        {
            inVbox.getChildren().add(inVbox.getChildren().size()-1,
                                     addPortField(block.addInput()));
        });
        
        Button inVboxDelButton = new Button("Odebrat vstup");
        inVboxDelButton.setOnAction(e ->
        {
            if (block.delInput() != null)
            {
                inVbox.getChildren().remove(block.getInputsNumber()+1);   
            }
        });
        
        HBox inButtonsHbox = new HBox(10, inVboxAddButton, inVboxDelButton);
        inButtonsHbox.setAlignment(Pos.CENTER);
        inVbox.getChildren().add(inButtonsHbox);
        
        return inVbox;
    }
    
    private VBox makeOutputs(Block block)
    {
        Text outVboxTitle = new Text("Výstupy");
        outVboxTitle.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        
        VBox outVbox = new VBox(outVboxTitle);
        outVbox.setPadding(new Insets(10));
        outVbox.setSpacing(8);
        outVbox.setMinWidth(300);
        outVbox.setAlignment(Pos.CENTER);
        
        
        block.getOutputs().forEach((output) -> {
            outVbox.getChildren().add(addPortField(output));
        });
        
        Button outVboxAddButton = new Button("Přidat výstup");
        outVboxAddButton.setOnAction(e ->
        {
            outVbox.getChildren().add(outVbox.getChildren().size()-1,
                                      addPortField(block.addOutput()));
        });
        
        Button outVboxDelButton = new Button("Odebrat výstup");
        outVboxDelButton.setOnAction(e ->
        {
            if (block.delOutput() != null)
            {
                outVbox.getChildren().remove(block.getOutputsNumber()+1);
            }
        });
        
        HBox outButtonsHbox = new HBox(10, outVboxAddButton, outVboxDelButton);
        outButtonsHbox.setAlignment(Pos.CENTER);
        outVbox.getChildren().add(outButtonsHbox);
        
        return outVbox;
    }
    
    private VBox makeEquations(Block block)
    {
        Text eqVboxTitle = new Text("Vzorce");
        eqVboxTitle.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        
        VBox eqVbox = new VBox(eqVboxTitle);
        eqVbox.setPadding(new Insets(10));
        eqVbox.setSpacing(8);
        eqVbox.setMinWidth(300);
        eqVbox.setAlignment(Pos.CENTER);
        eqVbox.setFillWidth(true);
        
        block.getEquations().forEach((eq) -> {
            eqVbox.getChildren().add(addEqArea(eq));
        });
        
        Button eqVboxAddButton = new Button("Přidat vzorec");
        eqVboxAddButton.setOnAction(e ->
        {
            TextArea eqArea = addEqArea(block.addEquation());
            eqVbox.getChildren().add(eqVbox.getChildren().size()-1, eqArea);
            eqArea.requestFocus();
        });
        
        Button eqVboxDelButton = new Button("Odebrat vzorec");
        eqVboxDelButton.setOnAction(e ->
        {
            if (block.delEquation() != null)
            {
                eqVbox.getChildren().remove(block.getEquations().size()+1);
            }
        });
        
        HBox eqButtonsHbox = new HBox(10, eqVboxAddButton, eqVboxDelButton);
        eqButtonsHbox.setAlignment(Pos.CENTER);
        eqVbox.getChildren().add(eqButtonsHbox);
        
        //eqVboxAddButton.setFocusTraversable(true);
        
        return eqVbox;
    }
    
    private HBox makeControlButtons(Block block)
    {
        Button exitButton = new Button("Zpět na schéma");
        exitButton.setOnAction(e -> 
        {
            block.redraw();
            // TODO block chceck
            manager.toSchema();
            block.equationsParse();
            block.checkInPortsCompatibity();
            block.checkOutPortsCompatibity();
            block.getGraphicRepresentation().requestFocus();
        });
        
        HBox box = new HBox(exitButton);
        box.setPadding(new Insets(10));
        box.setAlignment(Pos.TOP_RIGHT);
        
        return box;
    }
    
    private HBox makeBlockName(Block block)
    {
        TextField blockName = new TextField(block.introduce());
        blockName.textProperty().bindBidirectional(block.nameProperty());
        Text nameTitle = new Text("Název");
        nameTitle.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        HBox.setHgrow(blockName, Priority.ALWAYS);
        HBox box = new HBox(20, nameTitle, blockName);
        box.setPadding(new Insets(10));
        
        return box;
    }
    
    
    private HBox addPortField(Port port)
    {
        TextField portName = new TextField(port.introduce());
        portName.textProperty().bindBidirectional(port.nameProperty());
        
        HBox box = new HBox(8, portName);
        HBox.setHgrow(portName, Priority.ALWAYS);
        
        if (port.getConnection() != null)
        {
            Button Disconnect = new Button("Odpojit");
            box.getChildren().add(Disconnect);
            Disconnect.setOnAction(e ->
            {
                port.disconnect();
                box.getChildren().remove(Disconnect);
            });
        }
        
        return box;
    }
    
    private TextArea addEqArea(Equation eq)
    {
        TextArea eqArea = new TextArea(eq.getEquation());
        eqArea.textProperty().bindBidirectional(eq.equationProperty());
        eqArea.setWrapText(true);
        
        return eqArea;
    }
}