package app.gui.layouts.iterator;

import components.Block;
import java.util.List;

/**
 *
 * @author Antonín Bruštík
 */

public class BlockIterator
{
    private final List<Block> blocks;
    private int index = -1;
    
    // ------------------------------------------------------------------------
    public BlockIterator(List<Block> blocks)
    {
        this.blocks = blocks;
    }
    
    
    public boolean hasNext()
    {
        return this.index + 1 < this.blocks.size();
    }
    
    public Block next()
    {
        if (this.hasNext())
        {
            return this.blocks.get(++this.index);
        }
        
        return null;
    }
    
    
    public boolean hasActual()
    {
        return this.index != -1;
    }
    
    public Block actual()
    {
        if (this.hasActual())
        {
            return this.blocks.get(this.index);
        }
        
        return null;
    }
    
    
    public boolean hasPrevious()
    {
        return index > 0;
    }
    
    public Block previous()
    {
        if (this.hasPrevious())
        {
            return this.blocks.get(--this.index);
        }
        
        return null;
    }
}
