package app.gui;

import java.io.File;
import java.util.*;
import java.net.URL;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;

import app.gui.layouts.Schema;
import app.gui.layouts.Editor;
import components.Block;
import components.Connection;
import components.SchemaObject;
import components.math.exceptions.EmptyEquationException;
import components.math.exceptions.InvalidFormatException;
import components.math.exceptions.MissingVariableException;
import components.math.exceptions.UnknownMathException;
import components.xml.XmlRepresentation;

/**
 *
 * @author Antonín Bruštík
 */

public class Manager implements Initializable
{
    @FXML
    public ScrollPane sp;
    
    @FXML
    public VBox leftVbox;
    
    @FXML
    public VBox rightVbox;
    
    @FXML
    public ListView objectsList;
    
    private Stage stage = null;
    
    private final Schema schema = new Schema(this);
    private final Editor editor = new Editor(this);

    private int blockNumber = 0;
    
    private File file = null;
    
    
    // ------------------------------------------------------------------------

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        this.sp.setFitToHeight(true);
        this.sp.setFitToWidth(true);        
        sp.setFocusTraversable(false);
        toSchema();
    }
    
    
    
    // blocks -----------------------------------------------------------------
    public Block newBlock()
    {        
        return  newBlock(2, 2, 400, 200);
    }
    
    public Block newBlock(int inputNumber, int outputNumber)
    {
        return  newBlock(inputNumber, outputNumber, 400, 200);
    }
    
    public Block newBlock(int inputNumber, int outputNumber, double x, double y)
    {
        Block b = new Block("blok " + Integer.toString(++this.blockNumber), 
                            inputNumber, outputNumber);
        
        b.setManager(this);
        b.drawAndInsert(x, y);
        b.equationsParse();
        b.getGraphicRepresentation().requestFocus();
        
        this.setBlockAsLeaf(b);
        
        return  b;
    }
    
    
    
    // evaluation hierarchy ---------------------------------------------------
    public void setBlockAsLeaf(Block block)
    {
        this.schema.setBlockAsLeaf(block);
    }
    
    public void setBlockAsBranch(Block block)
    {
        this.schema.setBlockAsBranch(block);
    }
    
    
    
    // adding -----------------------------------------------------------------
    public void insertToSchema(Block object)
    {
        objectsList.getItems().add(object);
        schema.insertBlock(object);
    }
    
    public void insertToSchema(Connection object)
    {
        objectsList.getItems().add(object);
        schema.insertConnection(object);
    }
    
    public void insertToSchema(int index, Connection object)
    {
        objectsList.getItems().add(index, object);
        schema.insertConnection(object);
    }
    
    public void insertToSchema(int index, Block object)
    {
        objectsList.getItems().add(index, object);
        schema.insertBlock(object);
    }
    
    
    
    // removing ---------------------------------------------------------------
    public void removeFromSchema(SchemaObject object)
    {
        objectsList.getItems().remove(object);
        schema.remove(object);
    }
    
    
    
    // change layout ----------------------------------------------------------
    public void toEditor(Block block)
    {
        sp.setContent(editor.open(block));
    }
    
    public void toSchema()
    {
        sp.setContent(schema.open());
    }
    
    public void runSchema()
    {
        this.schema.run();
        this.leftVbox.setDisable(true);
    }
    
    public void editSchema()
    {
        this.schema.edit();
        this.leftVbox.setDisable(false);
    }
    
    
    
    // block numbering --------------------------------------------------------
    public void setBlockNumber(int number)
    {
        this.blockNumber = number;
    }
    
    
    
    // stage ------------------------------------------------------------------
    public void setStage(Stage stage)
    {
        this.stage = stage;
    }
    
    public Stage getStage()
    {
        return this.stage;
    }
    
    
    
    // port drag --------------------------------------------------------------
    public void portDraggedOverSchema()
    {
        schema.setDrawConnectionLine(e ->
        {
            Connection c = (Connection) e.getDragboard().getContent(Connection.FORMAT);

            if (c != null)
            {
                c.getGraphicRepresentation().setEnd(e.getX(), e.getY());
            }
        });
    }
    
    public void portDraggedOffSchema()
    {
        schema.setDrawConnectionLine(null);
    }

    
    
    
    // evaluation -------------------------------------------------------------
    public void evalAllBlocks()
    {
        if (this.schema.isMode(Schema.RUN))
        {
            while(this.schema.hasNextEvalBlock())
            {
                Block block = this.schema.getNextEvalBlock();
                this.evalBlock(block);
            }
        }
    }
    
    public void evalNextBlock()
    {
        if (this.schema.isMode(Schema.RUN) && this.schema.hasNextEvalBlock())
        {
            Block block = this.schema.getNextEvalBlock();
            this.evalBlock(block);
        }
    }
    
    public void evalPreviousBlock()
    {
        if (this.schema.isMode(Schema.RUN) && this.schema.hasPreviousEvalBlock())
        {
            Block block = this.schema.getPreviousEvalBlock();
            this.evalBlock(block);
        }
    }
    
    private void evalBlock(Block block)
    {
        try
        {
            this.schema.print(block.evaluate());
        }
        catch (MissingVariableException | InvalidFormatException |
               EmptyEquationException | UnknownMathException ex)
        {
            this.schema.print(ex.getResult());
            this.schema.print(ex.getMessage().concat("\n\n"));
        }
    }
    
    
    
    // open project -----------------------------------------------------------
    public void openProject()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otevřít projekt");
        fileChooser.getExtensionFilters().addAll(
            new ExtensionFilter("Schematix project", "*.sch"),
            new ExtensionFilter("All Files", "*.*"));
        File newFile = fileChooser.showOpenDialog(this.stage);
        
        if (newFile != null)
        {
            this.clearSchema();
            this.file = newFile;
            this.load(newFile);
        }
        
        this.editSchema();
    }
    
    private void load(File file)
    {
        try
        {
            XmlRepresentation xml;
            xml = new XmlRepresentation(this);
            xml.read(file);
            xml.loadFromXml();
                        
            System.out.println(file);
        }
        catch (Exception ex)
        {
            System.err.println("Error loading file");
        }
    }
    
    
    
    // save projekt -----------------------------------------------------------
    public void saveProjectAs()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Uložit projekt");
        fileChooser.setInitialFileName("schema.sch");
        fileChooser.getExtensionFilters().addAll(
            new ExtensionFilter("Schematix project", "*.sch"),
            new ExtensionFilter("All Files", "*.*"));
        File newFile = fileChooser.showSaveDialog(this.stage);

        if (newFile != null)
        {
            this.file = newFile;
            this.save(newFile);
        }
    }
    
    public void saveProject()
    {
        if (this.file == null)
        {
            this.saveProjectAs();
        }
        else
        {
            this.save(this.file);
        }
    }
    
    private void save(File file)
    {
        try
        {
            XmlRepresentation xml;
            xml = new XmlRepresentation(this);
            xml.saveToXml();
            xml.write(file);
            
            System.out.println(file);
        }
        catch (Exception ex)
        {
            System.err.println("Error saving file");
        }
    }
    
    
    
    // clear ------------------------------------------------------------------
    public void clearSchema()
    {
        this.objectsList.getItems().clear();
        this.schema.clear();        
        this.blockNumber = 0;
        this.file = null;
        this.editSchema();
    }
    
    
    
    // exit -------------------------------------------------------------------
    public void exit()
    {
        System.exit(0);
    }
}
