package graphic;

import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


/**
 *
 * @author Antonín Bruštík
 */

public abstract class GraphicRepresentation extends Group
{
    protected boolean forbidden = false;
            
    public GraphicRepresentation()
    {
        // bind
        this.focusedProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue)
            {
                this.focusActivate();
            }
            else
            {
                this.focusDeactivate();
            }
        });
        
        // events
        this.setOnMousePressed(e -> this.getFocus(e));
        this.setOnKeyPressed(e -> this.keyPressed(e));
    }
    
    public abstract void forbiddenActivate();
    public abstract void forbiddenDeactivate();
    
    public abstract void focusActivate();
    public abstract void focusDeactivate();
    
    public abstract void setSchemaRunMode();
    public abstract void setSchemaEditMode();
    
    public abstract void delete();
    
    
    public void getFocus(MouseEvent event)
    {
        this.requestFocus();
        event.consume();
    }
    
    public void keyPressed(KeyEvent event)
    {
        if(event.getCode() == KeyCode.DELETE)
        {
            this.delete();
        }
    }
}
