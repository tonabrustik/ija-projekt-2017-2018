package graphic;

import components.SchemaObject;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Antonín Bruštík
 */

public abstract class GSchemaObject extends Rectangle
{
    public GSchemaObject(double width, double height)
    {
        super(width, height);
    }
    
    public GSchemaObject(double x, double y, double width, double height)
    {
        super(x, y, width, height);
    }
    
    public abstract SchemaObject getObject();
    public abstract void setObject(SchemaObject object);
}
