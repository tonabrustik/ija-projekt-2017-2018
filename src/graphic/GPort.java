package graphic;

import javafx.beans.binding.Bindings;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;

import components.Port;
import components.Connection;
import components.SchemaObject;

/**
 *
 * @author Antonín Bruštík
 */

public class GPort extends GSchemaObject
{
    private Port port;
    
    private double offsetX = 0;
    private double offsetY = 0;
    
    private boolean isTargetPortCompatible = false;
    
    public GPort(Port port, double x, double y, double width, double height)
    {
        super(x, y, width, height);
        
        // set parent port
        this.port = port;
        
        // offsets
        this.offsetX = x == 0 ? 0 : x + width;
        this.offsetY = y + height / 2.0;
        
        // fill and border
        this.setFill(Color.BLACK);
        this.setStroke(Color.BLACK);

        // events
        this.setOnMouseEntered(e ->
        {
            this.setStroke(Color.WHITE);
        });

        this.setOnMouseExited(e ->
        {
            this.setStroke(Color.BLACK);
        });
        
        this.setOnMouseClicked(e ->  mouseClick(e));
        this.setOnDragDetected(e -> sourcePortDragged(e));
        this.setOnDragDone(e -> sourcePortDone(e));
        this.setOnDragOver(e -> targetPortOver(e));
        this.setOnDragDropped(e -> targetPortDropped(e));
        this.setOnDragEntered(e -> targetPortEntered(e));
        this.setOnDragExited(e -> targetPortExited(e));
    }
    
    public void setPort(Port port)
    {
        this.port = port;
    }
    
    public Port getPort()
    {
        return this.port;
    }

    @Override
    public Port getObject()
    {
        return port;
    }
    
    @Override
    public void setObject(SchemaObject object)
    {
        port = (Port) object;
    }
    
    private double getPortConnectionX()
    {
        return this.getParent().getLayoutX() + this.offsetX;
    }
    
    private double getPortConnectionY()
    {
        return this.getParent().getLayoutY() + this.offsetY;
    }
    
    public void bindEnd(Connection c)
    {
        c.getGraphicRepresentation().endXProperty().bind(
                Bindings.add(this.getParent().layoutXProperty(), this.offsetX));
        c.getGraphicRepresentation().endYProperty().bind(
                Bindings.add(this.getParent().layoutYProperty(), this.offsetY));
    }
    
    public void bindStart(Connection c)
    {
        c.getGraphicRepresentation().startXProperty().bind(
                Bindings.add(this.getParent().layoutXProperty(), this.offsetX));
        c.getGraphicRepresentation().startYProperty().bind(
                Bindings.add(this.getParent().layoutYProperty(), this.offsetY));
    }
    
    
    // events -----------------------------------------------------------------
    public void mouseClick(MouseEvent event)
    {
        if (event.getClickCount() == 2)
        {
            mouseDoubleClick(event);
        }
    }
    
    public void mouseDoubleClick(MouseEvent event)
    {
        if (event.getButton() == MouseButton.PRIMARY)
        {
            this.port.disconnect();
        }
    }
    
    public void sourcePortDragged(MouseEvent event)
    {
        this.port.disconnect();
        
        Connection c = new Connection();
        c.setManager(this.port.getBlock().getManager());
        c.drawAndInsert(this.getPortConnectionX(), this.getPortConnectionY());
        c.getManager().portDraggedOverSchema();
        
        this.port.connect(c);
        this.bindStart(c);
        
        ClipboardContent cb = new ClipboardContent();
        cb.put(Connection.FORMAT, c);
        
        Dragboard db = this.startDragAndDrop(TransferMode.MOVE);
        db.setContent(cb);
        
        event.consume();
    }
    
    
    public void targetPortOver(DragEvent event)
    {
        if (this.isTargetPortCompatible)
        {
            event.acceptTransferModes(TransferMode.MOVE);
            this.setStroke(Color.GREENYELLOW);
            this.setFill(Color.GREENYELLOW);
        }
        else
        {
            event.acceptTransferModes(TransferMode.NONE);
            this.setStroke(Color.RED);
            this.setFill(Color.RED);
        }
        
        event.consume();
    }
    
    public void targetPortEntered(DragEvent event)
    {
        Connection c = (Connection) event.getDragboard().getContent(Connection.FORMAT);
        
        if (c != null)
        {
            this.isTargetPortCompatible = this.port.isCompatibleAndFree(c)
                    && this.port.checkSourceBlockAncestors(c);
            
            // connection sticky behaviour
            c.getManager().portDraggedOffSchema();
            c.getGraphicRepresentation().setEnd(this.getPortConnectionX(),
                                                this.getPortConnectionY());
        }
        
        event.consume();
    }
    
    public void targetPortExited(DragEvent event)
    {
        this.setStroke(Color.BLACK);
        this.setFill(Color.BLACK);
        
        this.port.getBlock().getManager().portDraggedOverSchema();
        this.isTargetPortCompatible = false;
        
        event.consume();
    }
    
    public void targetPortDropped(DragEvent event)
    {
        Connection c = (Connection) event.getDragboard().getContent(Connection.FORMAT);
        this.port.connect(c);
        this.bindEnd(c);
        
        event.consume();
    }
    
    public void sourcePortDone(DragEvent event)
    {
        Connection c = this.port.getConnection();
        
        if (c != null)
        {
            if (!c.isConnected())
            {
                c.disconnect();
            }
            else
            {
                c.establish();
                c.getManager().portDraggedOffSchema();
            }
        }
        
        event.consume();
    }
}