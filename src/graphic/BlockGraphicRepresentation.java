package graphic;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.scene.input.MouseEvent;

import components.Block;
import components.InPort;
import components.OutPort;

/**
 *
 * @author Antonín Bruštík
 */

public class BlockGraphicRepresentation extends GraphicRepresentation
{
    public static final double BLOCK_WIDTH = 140;
    public static final double PORT_WIDTH  =  30;
    public static final double PORT_HEIGHT =  40;
    public static final double PORT_GAP    =  30;
    public static final double LABEL_GAP   =  10;
    
    private double offsetX = 0;
    private double offsetY = 0;
    
    private GBlock gBlock = null;
    private Rectangle redCover = null;

    // ------------------------------------------------------------------------
    
    public BlockGraphicRepresentation(Block block, double posX, double posY)
    {           
        // insert into group
        this.getChildren().addAll(makeObjects(block));   
        
        // red cover
        this.redCover = this.gBlock.makeRedCover();
        
        // position
        this.setLayoutX(posX);
        this.setLayoutY(posY);
    }
    
    
    private List<Node> makeObjects(Block block)
    {
        List<Node> objects = new ArrayList<>();
        
        Label blockName = new Label(block.introduce());
        //blockName.setMinWidth(Region.USE_PREF_SIZE);
        blockName.relocate(10, 10);
        objects.add(blockName);

        double in = PORT_GAP + LABEL_GAP;
        for (InPort input : block.getInputs())
        {
            GPort p = new GPort(input, 0,
                                in, PORT_WIDTH, PORT_HEIGHT);
            input.setGraphicRepresentation(p);
            p.setPort(input);
            objects.add(p);
            in += PORT_HEIGHT + PORT_GAP;
        }

        double out = PORT_GAP + LABEL_GAP;
        for (OutPort output : block.getOutputs())
        {
            GPort p = new GPort(output, BLOCK_WIDTH - PORT_WIDTH,
                                out, PORT_WIDTH, PORT_HEIGHT);
            output.setGraphicRepresentation(p);
            p.setPort(output);
            objects.add(p);
            out += PORT_HEIGHT + PORT_GAP;
        }

        this.gBlock = new GBlock(block, BLOCK_WIDTH, (in > out) ? in : out);
        
        //gBlock events
        this.gBlock.setOnMousePressed(e -> this.setPositionOffset(e));
        this.gBlock.setOnMouseDragged(e -> this.changePosition(e));
        
        objects.add(0, this.gBlock);
        
        return objects;
    }
    
    public void redraw(Block block)
    {
        this.getChildren().clear();
        this.getChildren().addAll(makeObjects(block));
    }
    
    @Override
    public void forbiddenActivate()
    {
        if(!this.forbidden)
        {
            this.getChildren().add(this.redCover);
            this.forbidden = true;
        }
    }
    
    @Override
    public void forbiddenDeactivate()
    {
        if(this.forbidden)
        {
            this.getChildren().remove(this.redCover);
            this.forbidden = false;
        }
    }
    
    @Override
    public void focusActivate()
    {
        this.gBlock.setFocused();
    }
    
    @Override
    public void focusDeactivate()
    {
        this.gBlock.setDefault();
    }
    
    public void setProcessed()
    {
        this.gBlock.setProcessed();
    }
    
    public void setDefault()
    {
        this.focusDeactivate();
        this.forbiddenDeactivate();
    }
    
    
    private void setPositionOffset(MouseEvent event)
    {
        this.offsetX = this.getLayoutX() - event.getSceneX();
        this.offsetY = this.getLayoutY() - event.getSceneY();
    }
    
    private void changePosition(MouseEvent event)
    {
        //this.setLayoutX(this.offsetX + event.getSceneX());
        //this.setLayoutY(this.offsetY + event.getSceneY());
        this.relocate(this.offsetX + event.getSceneX(),
                      this.offsetY + event.getSceneY());
        
        event.consume();
    }
    
    @Override
    public void getFocus(MouseEvent event)
    {
        this.requestFocus();
        event.consume();
    }
    
    
    @Override
    public void setSchemaRunMode()
    {
        this.setDisable(true);
    }
    
    
    @Override
    public void setSchemaEditMode()
    {
        this.setDisable(false);
    }
    
    
    @Override
    public void delete()
    {
        this.gBlock.getObject().delete();
    }
}
