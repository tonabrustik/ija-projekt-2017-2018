package graphic;

import javafx.beans.property.DoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.input.MouseEvent;

import components.Connection;

/**
 *
 * @author Antonín Bruštík
 */

public class ConnectionGraphicRepresentation extends GraphicRepresentation
{
    
    private final Line line;
    private final Connection connection;
    
    private final ConnectionDetail detail = new ConnectionDetail();
    
    public static final Color DEFAULTCOLOR = Color.GREY;
    public static final Color FOCUSCOLOR = Color.rgb(0xf0, 0xaa, 0x0);
    
    
    // ------------------------------------------------------------------------
    public ConnectionGraphicRepresentation(Connection connection,
                                           double startX, double startY,
                                           double endX, double endY)
    {
        this.connection = connection;
        
        // insert into group
        line = new Line(startX, startY, endX, endY);
        line.setStrokeWidth(7);
        line.setStroke(ConnectionGraphicRepresentation.DEFAULTCOLOR);
        this.getChildren().addAll(line);
        
        // invisible detail
        this.detail.close();
        this.getChildren().add(this.detail);
    }
    
    
    public void establish()
    {
        this.setOnMouseEntered(e -> this.showConnectionDetail(e));
        this.setOnMouseExited(e -> this.hideConnectionDetail(e));
    }
    
    
    // forbidden --------------------------------------------------------------
    @Override
    public void forbiddenActivate()
    {
        if(!this.forbidden)
        {
            //this.getChildren().add(this.redCover);
            this.forbidden = true;
        }
    }
    
    @Override
    public void forbiddenDeactivate()
    {
        if(this.forbidden)
        {
            //this.getChildren().remove(this.redCover);
            this.forbidden = false;
        }
    }
    
    @Override
    public void focusActivate()
    {
        line.setStroke(ConnectionGraphicRepresentation.FOCUSCOLOR);
    }
    
    @Override
    public void focusDeactivate()
    {
        line.setStroke(ConnectionGraphicRepresentation.DEFAULTCOLOR);
        this.detail.setVisible(false);
    }
    
    
    @Override
    public void setSchemaRunMode()
    {
        this.setOnMousePressed(null);
        this.setOnKeyPressed(null);
    }
    
    
    @Override
    public void setSchemaEditMode()
    {
        this.setOnMousePressed(e -> this.getFocus(e));
        this.setOnKeyPressed(e -> this.keyPressed(e));
    }
    
    
    // connection detail ------------------------------------------------------
    private void showConnectionDetail(MouseEvent event)
    {
        this.line.setStroke(ConnectionGraphicRepresentation.FOCUSCOLOR);
        
        double x = (this.line.getStartX() + this.line.getEndX()) / 2.0;
        double y = (this.line.getStartY() + this.line.getEndY()) / 2.0;
        
        this.detail.open(this.connection.getType());
        this.detail.setPosition(x, y);
    }
    
    private void hideConnectionDetail(MouseEvent event)
    {
        if (this.isFocused())
        {
            line.setStroke(ConnectionGraphicRepresentation.FOCUSCOLOR);
        }
        else
        {
            line.setStroke(ConnectionGraphicRepresentation.DEFAULTCOLOR);
            this.detail.close();
        }
    }
    
    
    // position ---------------------------------------------------------------
    public void setStartX(double x)
    {
        this.line.setStartX(x);
    }
    
    public void setStartY(double y)
    {
        this.line.setStartY(y);
    }
    
    public void setStart(double x, double y)
    {
        this.line.setStartX(x);
        this.line.setStartY(y);
    }
    
    public void setEndX(double x)
    {
        this.line.setEndX(x);
    }
    
    public void setEndY(double y)
    {
        this.line.setEndY(y);
    }
    
    public void setEnd(double x, double y)
    {
        this.line.setEndX(x);
        this.line.setEndY(y);
    }
    
    
    // properties -------------------------------------------------------------
    public DoubleProperty startXProperty()
    {
        return this.line.startXProperty();
    }
    
    public DoubleProperty startYProperty()
    {
        return this.line.startYProperty();
    }
    
    public DoubleProperty endXProperty()
    {
        return this.line.endXProperty();
    }
    
    public DoubleProperty endYProperty()
    {
        return this.line.endYProperty();
    }

    
    // delete -----------------------------------------------------------------
    @Override
    public void delete()
    {
        this.connection.delete();
    }
}
