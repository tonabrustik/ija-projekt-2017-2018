package graphic;

import java.util.Map;
import javafx.beans.binding.Bindings;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 * @author Antonín Bruštík
 */

public class ConnectionDetail extends Group
{
    
    // ------------------------------------------------------------------------
    public ConnectionDetail() {  }
    
    
    public void open(Map<String, Double> type)
    {
        TextFlow variables = this.makeText(type);
        Rectangle background = this.makeBackground(variables);
        
        this.getChildren().addAll(background, variables);
        this.setVisible(true);
    }
    
    
    public void close()
    {
        this.getChildren().clear();
        this.setVisible(false);
    }
    
    
    public void setPosition(double x, double y)
    {
        // enumerate of width and height
        this.applyCss();
        this.layout();
        
        this.relocate(x - (this.getBoundsInLocal().getWidth() / 2.0), 
                      y - (this.getBoundsInLocal().getHeight() / 2.0));
    }
    
    
    private TextFlow makeText(Map<String, Double> type)
    {
        TextFlow variables = new TextFlow();
        
        type.entrySet().forEach(row ->
        {
            Text variable = new Text(row.getKey() + ":  ");
            variable.setStyle("-fx-font-weight: bold;");
            Text value = new Text(row.getValue().toString());
            Text newLine = new Text("\n");
            variables.getChildren().addAll(variable, value, newLine);
            variables.setStyle("-fx-font-size: 14pt;");
        });
        
        variables.getChildren().remove(variables.getChildren().size() - 1); // delete last newLine
        variables.relocate(10, 10);
        
        return variables;
    }
    
    
    private Rectangle makeBackground(TextFlow variables)
    {
        Rectangle rec = new Rectangle(variables.getWidth(), variables.getHeight());
        rec.setFill(ConnectionGraphicRepresentation.FOCUSCOLOR);
        rec.setStroke(Color.BLACK);
        rec.setStrokeWidth(1);
        rec.setArcHeight(15);
        rec.setArcWidth(15);
        
        rec.heightProperty().bind(Bindings.add(variables.heightProperty(), 20));
        rec.widthProperty().bind(Bindings.add(variables.widthProperty(), 20));
        
        return rec;
    }
}
