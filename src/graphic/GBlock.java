package graphic;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import components.Block;
import components.SchemaObject;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Antonín Bruštík
 */

public class GBlock extends GSchemaObject
{
    private Block block;
    
    private static final Color DEFAULTCOLOR = Color.DODGERBLUE;
    private static final Color FOCUSCOLOR = Color.DODGERBLUE.darker();
    private static final Color PROCESSCOLOR = Color.rgb(0xf0, 0xaa, 0x0);
    
    
    public GBlock(Block block, double width, double height)
    {
        super(width, height);
        
        // set parent block
        this.block = block;

        // rounded edges
        this.setArcWidth(25);
        this.setArcHeight(25);
        
        // fill and border
        this.setFill(GBlock.DEFAULTCOLOR);
        this.setStroke(Color.BLACK);
        this.setStrokeWidth(2);
        
        // events   
        this.setOnMouseClicked(event ->  mouseClick(event));
    }
    
    public Rectangle makeRedCover()
    {
        Rectangle r = new Rectangle(getWidth(), getHeight());

        // rounded edges
        r.setArcWidth(getArcWidth());
        r.setArcHeight(getArcHeight());
        
        // fill and opacity
        r.setFill(Color.RED);
        r.setOpacity(0.5);
        
        return r;
    }

    @Override
    public Block getObject()
    {
        return block;
    }

    @Override
    public void setObject(SchemaObject object)
    {
        block = (Block) object;
    }
    
    
    public void setFocused()
    {
        this.setFill(GBlock.FOCUSCOLOR);
    }
    
    public void setProcessed()
    {
        this.setFill(GBlock.PROCESSCOLOR);
    }
    
    public void setDefault()
    {
        this.setFill(GBlock.DEFAULTCOLOR);
    }
    
    
    // events -----------------------------------------------------------------
    public void mouseClick(MouseEvent event)
    {
        if (event.getClickCount() == 2)
        {
            mouseDoubleClick(event);
        }
        
        event.consume();
    }
    
    public void mouseDoubleClick(MouseEvent event)
    {
        if (event.getButton() == MouseButton.PRIMARY)
        {
            this.block.getManager().toEditor(block);
        }
        
        event.consume();
    }
}
