package components;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;

/**
 *
 * @author Antonín Bruštík
 */

public abstract class SchemaObject implements SchemaObjectInterface
{
    private final StringProperty name = new SimpleStringProperty(this, "name", "");
    private Node img = null;
        
    @Override
    public String introduce()
    {
        return name.get();
    }
    
    @Override
    public final void rename(String name)
    {
        this.name.set(name);
    }
    
    public StringProperty nameProperty()
    {
        return name;
    }
    
    @Override
    public Node getGraphicRepresentation()
    {
        return this.img;
    }
    
    @Override
    public void setGraphicRepresentation(Node img)
    {
        this.img = img;
    }

    @Override
    public abstract void delete();
    
    @Override
    public String toString()
    {
        return this.introduce();
    }
}
