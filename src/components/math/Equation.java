package components.math;

import components.math.exceptions.EmptyEquationException;
import components.math.exceptions.InvalidFormatException;
import components.math.exceptions.MissingVariableException;
import components.math.exceptions.UnknownMathException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Antonín Bruštík
 */

public class Equation
{
    private static final Pattern VARPATTERN = Pattern.compile(
            "[a-zA-Z]+[a-zA-Z0-9_]*");
    
    private final StringProperty equation = new SimpleStringProperty(
            this, "equation", "");
    
    private String parsedEquation = "";
    private String evalEquation = "";
    
    private final List<String> parameters = new ArrayList<>();
    private String root = "";

    
    // ------------------------------------------------------------------------

    
    public Equation() {}

    public Equation(String eq)
    {
        this.setEquation(eq);
    }
    
    public void parse()
    {
        this.getReadyParse();
        
        Matcher matcher = VARPATTERN.matcher(this.parsedEquation);

        if (matcher.find())
        {            
            try
            {
                this.parsedEquation = this.parsedEquation.substring(
                        matcher.end() + 1);
            }
            catch (StringIndexOutOfBoundsException ex)
            {
                return;
            }
            
            this.root = matcher.group();
        }
        
        while (matcher.find())
        {
            this.parameters.add(matcher.group());
        }
    }
    
    private void getReadyParse()
    {
        this.root="";
        this.parameters.clear();
        this.parsedEquation = this.getEquation().replaceAll("\\s+","");
        this.evalEquation = "";
    }
    
    private void getReadyEvaluate()
    {
        this.evalEquation = this.parsedEquation;
    }
    
    public Double evaluate(Map<String, Double> values)
    {
        this.getReadyEvaluate();
        
        try
        {
            this.replaceParameters(values);
            String result = this.eval();
            
            System.out.println(this.evalEquation + " = " + result);
            
            return Double.parseDouble(result);
        }
        catch (MissingVariableException ex)
        {
            System.out.println(ex.getMessage()); // "Evaluate error - missing variable"
            return Double.NaN;
        }
        catch (NumberFormatException | ScriptException ex)
        {
            System.out.println("Evaluate error - invalid equation format");
            return Double.NaN;
        }
        catch (NullPointerException ex)
        {
            System.out.println("Evaluate error - empty equation");
            return Double.NaN;
        }
        catch (Exception ex)
        {
            System.out.println("Evaluate error");
            return Double.NaN;
        }
    }
    
    public Double evaluateWithThrow(Map<String, Double> values) throws
            MissingVariableException, InvalidFormatException,
            EmptyEquationException, UnknownMathException
    {
        this.getReadyEvaluate();
        
        try
        {
            this.replaceParameters(values);            
            return Double.parseDouble(this.eval());
        }
        catch (MissingVariableException ex)
        {
            throw ex;
        }
        catch (NumberFormatException | ScriptException ex)
        {
            throw new InvalidFormatException();
        }
        catch (NullPointerException ex)
        {
            throw new EmptyEquationException();
        }
        catch (Exception ex)
        {
            throw new UnknownMathException();
        }
    }
    
    
    public String getProcessedEquation()
    {
        return this.evalEquation;
    }
    
    public boolean testEvaluate()
    {
        this.getReadyEvaluate();
        this.replaceParameters("1");

        try
        {
            this.eval();
            return true;
        }
        catch (NumberFormatException | NullPointerException | ScriptException ex)
        {
            return false;
        }
    }
    
    private String eval() throws ScriptException
    {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        
        return engine.eval(this.evalEquation).toString();
    }
    
    private void replaceParameters(Map<String, Double> values) throws MissingVariableException
    {
        for (String p : this.parameters)
        {
            if (Double.isNaN(values.getOrDefault(p, Double.NaN)))
            {
                throw new MissingVariableException();
            }
            
            this.evalEquation = this.evalEquation.replaceAll(
                        p, values.get(p).toString()); // warning regex
        }
    }
    
    private void replaceParameters(String number)
    {
        this.parameters.forEach(p ->
        {
            this.evalEquation = this.evalEquation.replaceAll(
                    p, number); // warning regex
        });
    }
    
    public String getEquation()
    {
        return this.equation.get();
    }
    
    public final void setEquation(String eq)
    {
        this.equation.set(eq);
    }
    
    public StringProperty equationProperty()
    {
        return this.equation;
    }
    
    public List<String> getParameters()
    {
        return this.parameters;
    }
    
    public String getRoot()
    {
        return this.root;
    }
}