package components.math.exceptions;

/**
 *
 * @author Antonín Bruštík
 */

public class InvalidFormatException extends MathException
{
    public InvalidFormatException()
    {
        super("Evaluate error - invalid equation format");
    }
    
    public InvalidFormatException(String msg)
    {
        super(msg);
    }
}
