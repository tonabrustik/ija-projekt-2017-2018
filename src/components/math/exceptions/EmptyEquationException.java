package components.math.exceptions;

/**
 *
 * @author Antonín Bruštík
 */

public class EmptyEquationException extends MathException
{
    public EmptyEquationException()
    {
        super("Evaluate warning - empty equation");
    }
    
    public EmptyEquationException(String msg)
    {
        super(msg);
    }
}
