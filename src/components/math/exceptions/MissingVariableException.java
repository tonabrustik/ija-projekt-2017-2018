package components.math.exceptions;

/**
 *
 * @author Antonín Bruštík
 */

public class MissingVariableException extends MathException
{
    public MissingVariableException()
    {
        super("Evaluate error - missing variable");
    }
    
    public MissingVariableException(String msg)
    {
        super(msg);
    }
}
