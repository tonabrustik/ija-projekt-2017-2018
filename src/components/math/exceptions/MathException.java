package components.math.exceptions;

/**
 *
 * @author Antonín Bruštík
 */

public abstract class MathException extends Exception
{
    private String result;
    
    // ------------------------------------------------------------------------
    
    public MathException(String msg)
    {
        super(msg);
    }
    
    public void setResult(String result)
    {
        this.result = result;
    }
    
    public String getResult()
    {
        return this.result;
    }
}
