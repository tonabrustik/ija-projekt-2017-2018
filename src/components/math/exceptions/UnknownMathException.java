package components.math.exceptions;

/**
 *
 * @author Antonín Bruštík
 */

public class UnknownMathException extends MathException
{
    public UnknownMathException()
    {
        super("Evaluate error");
    }
    
    public UnknownMathException(String msg)
    {
        super(msg);
    }
}
