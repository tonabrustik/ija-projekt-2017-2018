package components;

/**
 *
 * @author Antonín Bruštík
 */
 
public interface DrawableInterface
{
    void draw(double posX, double posY);
    void redraw();
}
