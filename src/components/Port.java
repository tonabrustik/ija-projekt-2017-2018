package components;

import graphic.GPort;

/**
 *
 * @author Stanislav Bartoš
 */

public abstract class Port extends SchemaObject
{
    private Connection connection = null;
    private final Block block;

    // ----------------------------------------------------------------
    
    public Port(Block block, String name)
    {
        rename(name);
        this.block = block;
    }
    
    public Block getBlock()
    {
        return this.block;
    }

    public void connect(Connection c)
    {
        this.connection = c;
    }

    public void disconnect()
    {
        if (this.getConnection() != null)
        {
            this.getConnection().disconnect();
        }
    }
    
    public void setConnectionNull()
    {
        this.connection = null;
    }

    public Connection getConnection()
    {
        return this.connection;
    }
    
    public boolean isAvailable()
    {
        return this.getConnection() == null;
    }

    public abstract boolean isCompatible(Port otherPort);
    public abstract boolean isStillCompatible();
    
    public boolean isCompatible(Connection c)
    {
        Port p = this.getConnectedPort(c);
        
        return p == null ? false : this.isCompatible(p);
    }
    
    public boolean isCompatibleAndFree(Port otherPort)
    {
        return this.isCompatible(otherPort) && this.isAvailable();
    }
    
    public boolean isCompatibleAndFree(Connection c)
    {
        return this.isCompatible(c) && this.isAvailable();
    }
    
    public abstract boolean checkSourceBlockAncestors(Connection c);
    
    public abstract Port getConnectedPort();
    public abstract Port getConnectedPort(Connection c);
    
    
    @Override
    public GPort getGraphicRepresentation()
    {
        return (GPort) super.getGraphicRepresentation();
    }
    
    @Override
    public void delete()
    {
        if (this.connection != null)
        {
            this.connection.delete();
        }
    }
}
