package components;

import app.gui.Manager;
import graphic.ConnectionGraphicRepresentation;
import java.util.Map;
import javafx.scene.input.DataFormat;

/**
 *
 * @author Stanislav Bartoš
 */

public class Connection extends SchemaObject implements DrawableInterface
{
    public static final DataFormat FORMAT = DataFormat.RTF;
    
    private InPort to = null;
    private OutPort from = null;
    
    private Manager manager = null;
    
    // ------------------------------------------------------------------------

    public Connection() {  }
    
    public Connection(String name)
    {
        rename(name);
    }

    public Connection(InPort to, OutPort from)
    {
        if (to != null && from != null && to.isCompatible(from))
        {
            this.to = to;
            this.from = from;
        }
    }

    public Connection(String name, InPort to, OutPort from)
    {
        rename(name);
        if (to != null && from != null && to.isCompatible(from))
        {
            this.to = to;
            this.from = from;
        }
    }
    
    public void setTo(InPort p)
    {
        if (this.to == null)
        {
            this.to = p;
        }
    }

    public Port to()
    {
        return to;
    }
    
    public void setFrom(OutPort p)
    {
        if (this.from == null)
        {
            this.from = p;
        }
    }

    public Port from()
    {
        return from;
    }
    
    public Map<String, Double> getType()
    {
        if (this.from != null)
        {
            return this.from.getBlock().getOutValues();
        }
        
        return null;
    }
    
    public boolean isConnected()
    {
        return (this.to != null) && (this.from != null);
    }
    
    public void disconnect()
    {
        if (this.to != null)
        {
            this.to.getBlock().delInConnection();
            this.to.setConnectionNull();
            this.to = null;
        }
        
        if (this.from != null)
        {
            this.from.getBlock().delOutConnection();
            this.from.setConnectionNull();
            this.from = null;
        }
        
        this.getManager().removeFromSchema(this);
        
        this.getGraphicRepresentation().startXProperty().unbind();
        this.getGraphicRepresentation().startYProperty().unbind();
        this.getGraphicRepresentation().endXProperty().unbind();
        this.getGraphicRepresentation().endYProperty().unbind();
    }
    
    public void establish()
    {
        this.to.getBlock().addInConnection();
        this.from.getBlock().addOutConnection();
        
        if (this.introduce().equals(""))
        {
            this.rename("spojení " + this.from.getBlock().introduce()
                          + " -> " + this.to.getBlock().introduce());
        }
        
        this.getGraphicRepresentation().establish();
    }
    
    
    // graphic ----------------------------------------------------------------
    @Override
    public void draw(double posX, double posY)
    {
        this.setGraphicRepresentation(new ConnectionGraphicRepresentation(
                this, posX, posY, posX, posY));
    }
    
    public void drawAndInsert(double posX, double posY)
    {
        this.draw(posX, posY);
        this.getManager().insertToSchema(this);
    }
    
    public void drawAndInsert(double posX, double posY, int index)
    {
        this.draw(posX, posY);
        this.getManager().insertToSchema(index, this);
    }
    
    @Override
    public void redraw()
    {
        //
    }
    
    @Override
    public ConnectionGraphicRepresentation getGraphicRepresentation()
    {
        return (ConnectionGraphicRepresentation) super.getGraphicRepresentation();
    }
    
    
    
    @Override
    public void delete()
    {
        this.disconnect();
    }
    

    
    
    // manager ----------------------------------------------------------------
    public void setManager(Manager manager)
    {
        this.manager = manager;
    }

    public Manager getManager()
    {
        return this.manager;
    }
}
