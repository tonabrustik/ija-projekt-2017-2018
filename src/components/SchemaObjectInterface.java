package components;

import javafx.scene.Node;

/**
 *
 * @author Antonín Bruštík
 */
 
public interface SchemaObjectInterface
{
    String introduce();
    void rename(String name);
    Node getGraphicRepresentation();
    void setGraphicRepresentation(Node img);
    void delete();
}
