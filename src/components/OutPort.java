package components;

import java.util.*;

/**
 *
 * @author Stanislav Bartoš
 */

public class OutPort extends Port
{
    // ------------------------------------------------------------------------

    public OutPort(Block block, String name)
    {
        super(block, name);
    }

    @Override
    public Port getConnectedPort()
    {
        return this.getConnection() == null ? null : this.getConnection().to();
    }
    
    @Override
    public Port getConnectedPort(Connection c)
    {
        return c.to();
    }
    
    @Override
    public void connect(Connection c)
    {
        super.connect(c);
        c.setFrom(this);
    }
    
    @Override
    public boolean isCompatible(Port otherPort)
    {
        if (otherPort.getClass().equals(this.getClass()))
        {
            return false;
        }
        
        InPort inPort = (InPort) otherPort;
        return !this.getBlockProvidedOutput().isEmpty() &&
                inPort.getBlockRequiredInput().containsAll(
                        this.getBlockProvidedOutput());
    }
    
    @Override
    public boolean isStillCompatible()
    {
        InPort inPort = (InPort) this.getConnectedPort();
        
        return !this.getBlockProvidedOutput().isEmpty() &&
                inPort.getBlockAllRequiredInput().containsAll(
                        this.getBlockProvidedOutput());
    }
    
    @Override
    public boolean checkSourceBlockAncestors(Connection c)
    {
        return this.getBlock().checkLoops(this.getConnectedPort(c).getBlock());
    }

    
    public final Set<String> getBlockProvidedOutput()
    {
        return this.getBlock().getProvidedOutput();
    }
}
