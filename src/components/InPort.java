package components;

import java.util.*;

/**
 *
 * @author Stanislav Bartoš
 */

public class InPort extends Port
{
    // ------------------------------------------------------------------------

    public InPort(Block block, String name)
    {
        super(block, name);
    }

    @Override
    public Port getConnectedPort()
    {
        return this.getConnection() == null ? null : this.getConnection().from();
    }
    
    @Override
    public Port getConnectedPort(Connection c)
    {
        return c.from();
    }
    
    @Override
    public void connect(Connection c)
    {
        super.connect(c);
        c.setTo(this);
    }
    
    @Override
    public boolean isCompatible(Port otherPort)
    {
        if (otherPort.getClass().equals(this.getClass()))
        {
            return false;
        }
        
        OutPort outPort = (OutPort) otherPort;
        return !outPort.getBlockProvidedOutput().isEmpty() &&
                this.getBlockRequiredInput().containsAll(
                        outPort.getBlockProvidedOutput());
    }
    
    @Override
    public boolean isStillCompatible()
    {
        OutPort outPort = (OutPort) this.getConnectedPort();
        
        return !outPort.getBlockProvidedOutput().isEmpty() &&
                this.getBlockAllRequiredInput().containsAll(
                        outPort.getBlockProvidedOutput());
    }
    
    @Override
    public boolean checkSourceBlockAncestors(Connection c)
    {
        return this.getConnectedPort(c).getBlock().checkLoops(this.getBlock());
    }
    
    public Set<String> getBlockRequiredInput()
    {
        return this.getBlock().getRequiredInput();
    }
    
    public Set<String> getBlockAllRequiredInput()
    {
        return this.getBlock().getAllRequiredInput();
    }
}
