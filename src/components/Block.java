package components;

import java.util.*;
import java.util.stream.Collectors;

import app.gui.Manager;
import components.math.exceptions.EmptyEquationException;
import components.math.Equation;
import components.math.exceptions.InvalidFormatException;
import components.math.exceptions.MissingVariableException;
import components.math.exceptions.UnknownMathException;
import graphic.BlockGraphicRepresentation;

/**
 *
 * @author Antonín Bruštík
 */

public class Block extends SchemaObject implements DrawableInterface
{
    private final List<InPort> inputs;
    private final List<OutPort> outputs;

    private final List<Equation> equations;

    private final Set<String> requiredInput = new HashSet<>();
    private final Map<String, Double> outValues = new HashMap<>();
    
    private int inConnectionCount = 0;
    private int outConnectionCount = 0;

    private Manager manager = null;


    // ------------------------------------------------------------------------


    public Block(String name)
    {
        rename(name);
        inputs = new ArrayList<>();
        outputs = new ArrayList<>();
        equations = new ArrayList<>();
    }

    public Block(String name, int inputsNumber, int outputsNumber)
    {
        rename(name);
        inputs = new ArrayList<>();
        outputs = new ArrayList<>();
        equations = new ArrayList<>();

        for(int i = 0; i < inputsNumber; ++i)
            addInput();

        for(int i = 0; i < outputsNumber; ++i)
            addOutput();
    }

    public Block(String name, List<InPort> inputs, List<OutPort> outputs)
    {
        rename(name);
        this.inputs = inputs;
        this.outputs = outputs;
        equations = new ArrayList<>();
    }



    // inPorts ----------------------------------------------------------------
    public InPort addInput()
    {
        String name = "Vstup " + Integer.toString(inputs.size() + 1);
        InPort p = new InPort(this, name);
        return (inputs.add(p)) ? p : null;
    }

    public InPort addInput(String name)
    {
        InPort p = new InPort(this, name);
        return (inputs.add(p)) ? p : null;
    }

    public boolean delInput(InPort p)
    {
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return inputs.remove(p);
    }

    public InPort delInput(int index)
    {
        InPort p = inputs.remove(index);
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return p;
    }

    public InPort delInput()
    {
        if (inputs.size() < 1)
        {
            return null;
        }

        InPort p = inputs.remove(inputs.size()-1);
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return p;
    }

    public InPort getInput(int index)
    {
        return inputs.get(index);
    }

    public List<InPort> getInputs()
    {
        return inputs;
    }

    public int getInputsNumber()
    {
        return inputs.size();
    }



    // outPorts ---------------------------------------------------------------
    public OutPort addOutput()
    {
        String name = "Výstup " + Integer.toString(outputs.size() + 1);
        OutPort p = new OutPort(this, name);
        return (outputs.add(p)) ? p : null;
    }

    public OutPort addOutput(String name)
    {
        OutPort p = new OutPort(this, name);
        return (outputs.add(p)) ? p : null;
    }

    public boolean delOutput(OutPort p)
    {
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return outputs.remove(p);
    }

    public OutPort delOutput(int index)
    {
        OutPort p = outputs.remove(index);
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return p;
    }

    public OutPort delOutput()
    {
        if (outputs.size() < 1)
        {
            return null;
        }

        OutPort p = outputs.remove(outputs.size()-1);
        if (p.getConnection() != null)
        {
            p.getConnection().disconnect();
        }
        return p;
    }

    public OutPort getOutput(int index)
    {
        return outputs.get(index);
    }

    public List<OutPort> getOutputs()
    {
        return outputs;
    }

    public int getOutputsNumber()
    {
        return outputs.size();
    }



    // graphic ----------------------------------------------------------------
    @Override
    public void draw(double posX, double posY)
    {
        this.setGraphicRepresentation(
                new BlockGraphicRepresentation(this, posX, posY));
    }
    
    public void drawAndInsert(double posX, double posY)
    {
        this.draw(posX, posY);
        this.getManager().insertToSchema(this);
    }
    
    public void drawAndInsert(double posX, double posY, int index)
    {
        this.draw(posX, posY);
        this.getManager().insertToSchema(index, this);
    }

    @Override
    public void redraw()
    {
        this.getGraphicRepresentation().redraw(this);
    }
    
    @Override
    public BlockGraphicRepresentation getGraphicRepresentation()
    {
        return (BlockGraphicRepresentation) super.getGraphicRepresentation();
    }



    // math -------------------------------------------------------------------
    public Equation addEquation()
    {
        Equation e = new Equation();
        return equations.add(e) ? e : null;
    }

    public Equation addEquation(String eq)
    {
        Equation e = new Equation(eq);
        return equations.add(e) ? e : null;
    }

    public Equation delEquation()
    {
        if (this.equations.size() < 1)
        {
            return null;
        }
        return this.equations.remove(this.equations.size()-1);
    }

    public List<Equation> getEquations()
    {
        return this.equations;
    }

    public void equationsParse()
    {
        this.requiredInput.clear();
        this.outValues.clear();

        this.equations.forEach(eq ->
        {
            eq.parse();
            eq.getParameters().forEach(this.requiredInput::add);

            if (!eq.getRoot().equals(""))
            {
                this.outValues.put(eq.getRoot(), Double.NaN);
            }
        });

        this.outValues.keySet().forEach(this.requiredInput::remove);
    }
    
    


    // input ------------------------------------------------------------------
    public Map<String, Double> getInValues()
    {
        Map<String, Double> inValues = new HashMap<>();

        this.inputs.stream().filter((p) -> (p.getConnectedPort() != null)).forEach((p) ->
        {
            inValues.putAll(p.getConnectedPort().getBlock().getOutValues());
        });

        return inValues;
    }

    public Set<String> getRequiredInput()
    {
        return this.requiredInput.stream().filter((p) -> (
                !this.getInValues().containsKey(p))).collect(Collectors.toSet());
    }

    public Set<String> getAllRequiredInput()
    {
        return this.requiredInput;
    }

    public boolean checkInput()
    {
        return this.requiredInput.stream().map((p) -> (
                this.getInValues().containsKey(p))).reduce(true, (x, y) -> (x && y));
    }
    
    
    // output -----------------------------------------------------------------
    public Map<String, Double> getOutValues()
    {
        return this.outValues;
    }
    
     public Set<String> getProvidedOutput()
    {
        return this.outValues.keySet();
    }



    // after equation parsing -------------------------------------------------
    public void checkInPortsCompatibity()
    {
        this.inputs.stream().filter((p) -> (
                 p.getConnectedPort() != null)).forEach((p) ->
        {
            if (!p.isStillCompatible())
            {
                p.disconnect();
            }
        });
    }
    
    public void checkOutPortsCompatibity()
    {
        this.outputs.stream().filter((p) -> (
                 p.getConnectedPort() != null)).forEach((p) ->
        {
            if (!p.isStillCompatible())
            {
                p.disconnect();
            }
        });
    }
    
    


    // loops detection --------------------------------------------------------
    public boolean checkLoops(Block block)
    {
        if (block == this)
        {
            return false;
        }

        return this.inputs.stream().map(InPort::getConnectedPort).noneMatch(
                (otherPort) -> (otherPort != null && !otherPort.getBlock().checkLoops(block)));
    }



    // connections ------------------------------------------------------------
    public int getInConnectionCount()
    {
        return this.inConnectionCount;
    }

    public void addInConnection()
    {
        this.inConnectionCount++;
    }

    public void delInConnection()
    {
        this.inConnectionCount--;
    }

    public int getOutConnectionCount()
    {
        return this.outConnectionCount;
    }

    public void addOutConnection()
    {
        if (++this.outConnectionCount == 1)
        {
            this.getManager().setBlockAsBranch(this);
        }
    }

    public void delOutConnection()
    {
        if (--this.outConnectionCount == 0)
        {
            this.getManager().setBlockAsLeaf(this);
        }
    }



    // block process ----------------------------------------------------------
    public String evaluate() throws MissingVariableException, InvalidFormatException,
                                  EmptyEquationException, UnknownMathException
    {
        Map<String, Double> inValuesLocal = new HashMap<>(this.getInValues());
        
        String resultString = this.introduce().concat("\n");
        
        for (Equation eq : this.getEquations())
        {
            try
            {
                Double result = eq.evaluateWithThrow(inValuesLocal);
                
                inValuesLocal.put(eq.getRoot(), result);
                this.outValues.replace(eq.getRoot(), result);
                
                resultString = resultString.concat(
                        eq.getRoot() + " = " + result + "\n");
            }
            catch (MissingVariableException | InvalidFormatException |
                   EmptyEquationException | UnknownMathException ex)
            {
                this.outValues.replace(eq.getRoot(), Double.NaN);
                ex.setResult(resultString);
                throw ex;
            }
        }
        
        return resultString.concat("\n");
    }


    
    // return direct ancestors ------------------------------------------------
    public List<Block> getAncestors()
    {
        return this.getInputs().stream().filter(port -> (
                port.getConnectedPort() != null)).map(port -> (
                        port.getConnectedPort().getBlock())).collect(Collectors.toList());
    }
    
    
    
    
    // return direct succesors ------------------------------------------------
    public List<Block> getSuccessors()
    {
        return this.getOutputs().stream().filter(port -> (
                port.getConnectedPort() != null)).map(port -> (
                        port.getConnectedPort().getBlock())).collect(Collectors.toList());
    }
    

    
    
    // graphic control --------------------------------------------------------
    public void setDefault()
    {
        this.getGraphicRepresentation().setDefault();
    }
    
    public void setHighlight()
    {
        this.getGraphicRepresentation().setProcessed();
    }


    
    
    // manager ----------------------------------------------------------------
    public void setManager(Manager manager)
    {
        this.manager = manager;
    }

    public Manager getManager()
    {
        return this.manager;
    }
    
    
    
    
    // destructor -------------------------------------------------------------
    @Override
    public void delete()
    {
        this.inputs.forEach(InPort::delete);
        this.outputs.forEach(OutPort::delete);
        this.getManager().removeFromSchema(this);
    }
}