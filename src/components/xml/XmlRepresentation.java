package components.xml;

import app.gui.Manager;
import components.Block;
import components.Connection;
import components.InPort;
import components.OutPort;
import components.Port;
import components.SchemaObject;
import components.math.Equation;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 *
 * @author Antonín Bruštík
 */

public class XmlRepresentation
{
    private Document document = null;
    private final Manager manager;
    
    // ------------------------------------------------------------------------
    
    public XmlRepresentation(Manager manager)
    {
        this.manager = manager;
    }
    
    
    
    // objects ----------------------------------------------------------------
    private List<SchemaObject> getObjects()
    {
        return this.manager.objectsList.getItems();
    }
    
    private SchemaObject getObject(int index)
    {
        return (SchemaObject) this.manager.objectsList.getItems().get(index);
    }
    
    private List<Block> getBlocks()
    {
        return this.manager.objectsList.getItems().filtered(
                item -> item instanceof Block);
    }
    
    private Block getBlock(int index)
    {
        return (Block) this.manager.objectsList.getItems().filtered(
                item -> item instanceof Block).get(index);
    }
    
    private List<Connection> getConnections()
    {
        return this.manager.objectsList.getItems().filtered(
                item -> item instanceof Connection);
    }
    
    private Connection getConnection(int index)
    {
        return (Connection) this.manager.objectsList.getItems().filtered(
                item -> item instanceof Connection).get(index);
    }
    
    
    
    // saving -----------------------------------------------------------------
    public void saveToXml() throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        this.document = builder.newDocument();
        
        // schema - root element
        Element schema = this.createElement("Schema");
        this.document.appendChild(schema);
        
        // blocks
        Element blocks = this.createElement("Blocks");
        schema.appendChild(blocks);
        
        // connections
        Element connections = this.createElement("Connections");
        schema.appendChild(connections);
        
        for (int i = 0; i < this.getBlocks().size(); ++i)
        {
            this.saveBlockToXml(this.getBlock(i), i, blocks);
        }
        
        for (int i = 0; i < this.getConnections().size(); ++i)
        {
            this.saveConnectionToXml(this.getConnection(i), i, connections);
        }
    }
    
    
    private void saveBlockToXml(Block block, int id, Element rootElement)
    {
        Element blockElement = this.createElement("Block");
        blockElement.setAttribute("id", Integer.toString(id));
        rootElement.appendChild(blockElement);
        
        // name
        Element name = this.createElement("name");
        name.appendChild(this.createTextNode(block.introduce()));
        blockElement.appendChild(name);
        
        // position x
        Element x = this.createElement("x");
        x.appendChild(this.createTextNode(
                Double.toString(block.getGraphicRepresentation().getLayoutX())));
        blockElement.appendChild(x);
        
        // position y
        Element y = this.createElement("y");
        y.appendChild(this.createTextNode(
                Double.toString(block.getGraphicRepresentation().getLayoutY())));
        blockElement.appendChild(y);
        
        // inports
        Element inports = this.createElement("InPorts");
        blockElement.appendChild(inports);
        
        for (int i = 0; i < block.getInputsNumber(); ++i)
        {
            this.savePortToXml(block.getInput(i), i, inports);
        }
        
        // outports
        Element outports = this.createElement("OutPorts");
        blockElement.appendChild(outports);
        
        for (int i = 0; i < block.getOutputsNumber(); ++i)
        {
            this.savePortToXml(block.getOutput(i), i, outports);
        }
        
        // equations
        Element equations = this.createElement("Equations");
        blockElement.appendChild(equations);
        
        block.getEquations().forEach(eq ->
        {
            this.saveEquationToXml(eq, equations);
        });
    }
    
    
    private void savePortToXml(Port port, int id, Element rootElement)
    {
        Element portElement = this.createElement("Port");
        portElement.setAttribute("id", Integer.toString(id));
        rootElement.appendChild(portElement);
        
        Element name = this.createElement("name");
        name.appendChild(this.createTextNode(port.introduce()));
        portElement.appendChild(name);
    }
    
    
    private void saveEquationToXml(Equation eq, Element rootElement)
    {
        Element eqElement = this.createElement("Equation");
        eqElement.appendChild(this.createTextNode(eq.getEquation()));
        rootElement.appendChild(eqElement);
    }

    
    private void saveConnectionToXml(Connection connection, int id, Element rootElement)
    {
        Element connectionElement = this.createElement("Connection");
        connectionElement.setAttribute("id", Integer.toString(id));
        rootElement.appendChild(connectionElement);
        
        // name
        Element name = this.createElement("name");
        name.appendChild(this.createTextNode(connection.introduce()));
        connectionElement.appendChild(name);
        
        // original index
        Element index = this.createElement("index");
        index.appendChild(this.createTextNode(Integer.toString(
                this.getObjects().indexOf(connection))));
        connectionElement.appendChild(index);
        
        // source block
        Element sourceBlock = this.createElement("sourceblock");
        sourceBlock.appendChild(this.createTextNode(Integer.toString(
                this.getBlocks().indexOf(connection.from().getBlock()))));
        connectionElement.appendChild(sourceBlock);
        
        // source port
        Element sourcePort = this.createElement("sourceport");
        sourcePort.appendChild(this.createTextNode(
                Integer.toString(connection.from().getBlock().getOutputs()
                        .indexOf(connection.from()))));
        connectionElement.appendChild(sourcePort);
        
        // target block
        Element targetBlock = this.createElement("targetblock");
        targetBlock.appendChild(this.createTextNode(Integer.toString(
                this.getBlocks().indexOf(connection.to().getBlock()))));
        connectionElement.appendChild(targetBlock);
        
        // target port
        Element targetPort = this.createElement("targetport");
        targetPort.appendChild(this.createTextNode(Integer.toString(
                connection.to().getBlock().getInputs()
                        .indexOf(connection.to()))));
        connectionElement.appendChild(targetPort);
    }
    
    private Element createElement(String name)
    {
        return this.document.createElement(name);
    }
    
    private Text createTextNode(String name)
    {
        return this.document.createTextNode(name);
    }
    
    
    public void write(File outfile) throws TransformerException
    {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");

        DOMSource source = new DOMSource(this.document);

        StreamResult result = new StreamResult(outfile);
        transformer.transform(source, result);
    }
    
    
    
    
    // loading ----------------------------------------------------------------
    public void loadFromXml()
    {
        // schema - root element
        Element schema = this.document.getDocumentElement();

        // blocks
        Element blocks = this.getElementFromElement(schema, "Blocks");
        this.manager.setBlockNumber(this.getElementLength(blocks, "Block"));
        
        for (int i = 0; i < this.getElementLength(blocks, "Block"); ++i)
        {
            this.loadBlockFromXml(this.getElementFromElement(
                    blocks, "Block", i));
        }
        
        // connections
        Element connections = this.getElementFromElement(schema, "Connections");
        
        for (int i = 0; i < this.getElementLength(connections, "Connection"); ++i)
        {
            this.loadConnectionFromXml(this.getElementFromElement(
                    connections, "Connection", i));
        }
    }
    
    
    private void loadBlockFromXml(Element blockElement)
    {
        Block block = new Block(this.getStringFromElement(blockElement, "name"));
        
        // inports
        Element inports = this.getElementFromElement(blockElement, "InPorts");
        
        for (int i = 0; i < this.getElementLength(inports, "Port"); ++i)
        {
            this.loadInPortFromXml(this.getElementFromElement(
                    inports, "Port", i), block);
        }
        
        // outports
        Element outports = this.getElementFromElement(blockElement, "OutPorts");
        
        for (int i = 0; i < this.getElementLength(outports, "Port"); ++i)
        {
            this.loadOutPortFromXml(this.getElementFromElement(
                    outports, "Port", i), block);
        }
        
        // equations
        Element equations = this.getElementFromElement(blockElement, "Equations");
        
        for (int i = 0; i < this.getElementLength(equations, "Equation"); ++i)
        {
            this.loadEquationFromXml(this.getElementFromElement(
                    equations, "Equation", i), block);
        }
        
        this.manager.setBlockAsLeaf(block);
        block.setManager(this.manager);
        block.equationsParse();
        block.drawAndInsert(this.getDoubleFromElement(blockElement, "x"),
                            this.getDoubleFromElement(blockElement, "y"));
    }
    
    
    private void loadInPortFromXml(Element portElement, Block block)
    {
        block.addInput(this.getStringFromElement(portElement, "name"));
    }
    
    
    private void loadOutPortFromXml(Element portElement, Block block)
    {
        block.addOutput(this.getStringFromElement(portElement, "name"));
    }
    
    
    private void loadEquationFromXml(Element eqElement, Block block)
    {
        block.addEquation(eqElement.getTextContent());
    }

    
    private void loadConnectionFromXml(Element connectionElement)
    {
        OutPort outport = this.getBlock(this.getIntegerFromElement(
                connectionElement, "sourceblock")).getOutput(
                        this.getIntegerFromElement(connectionElement, "sourceport"));
        
        InPort inport = this.getBlock(this.getIntegerFromElement(
                connectionElement, "targetblock")).getInput(
                        this.getIntegerFromElement(connectionElement, "targetport"));
        
        Connection c = new Connection(this.getStringFromElement(connectionElement, "name"));
        c.setManager(this.manager);
        c.drawAndInsert(0, 0, this.getIntegerFromElement(connectionElement, "index"));
        
        inport.connect(c);
        outport.connect(c);
        
        outport.getGraphicRepresentation().bindStart(c);
        inport.getGraphicRepresentation().bindEnd(c);
        
        c.establish();
    }
    
    
    public void read(File infile) throws
            SAXException, IOException, ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        this.document = builder.parse(infile);
    }
    
    private int getElementLength(Element element, String attribute)
    {
        return element.getElementsByTagName(attribute).getLength();
    }
    
    private Element getElementFromElement(Element element, String attribute, int index)
    {
        return (Element) element.getElementsByTagName(attribute).item(index);
    } 
    
    private Element getElementFromElement(Element element, String attribute)
    {
        return (Element) element.getElementsByTagName(attribute).item(0);
    } 
    
    private String getStringFromElement(Element element, String attribute)
    {
        return element.getElementsByTagName(attribute).item(0).getTextContent();
    }
    
    private int getIntegerFromElement(Element element, String attribute)
    {
        return Integer.parseInt(element.getElementsByTagName(
                attribute).item(0).getTextContent());
    }
    
    private double getDoubleFromElement(Element element, String attribute)
    {
        return Double.parseDouble(element.getElementsByTagName(
                attribute).item(0).getTextContent());
    }
}