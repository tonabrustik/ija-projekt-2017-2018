package tests;

import components.*;

import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Antonín Bruštík
 */

public class SchemaObjectsTest
{
    private Block block1;
    private Block block2;
    private Block block3;
    private Block block4;
    private SchemaObject object1;


    @Before
    public void setUp()
    {
        block1 = new Block("blok 1");
        block2 = new Block("blok 2", 4, 3);
        block3 = new Block("blok 3", 2, 2);
        block4 = new Block("b4");
        object1 = new Block("blok 5");
    }

    /**
     * Kontrola jmen objektu.
     */
    @Test
    public void test01()
    {
        Assert.assertEquals("Test nazvu.", "blok 1", block1.introduce());
        Assert.assertEquals("Test nazvu.", "blok 3", block3.introduce());
        Assert.assertEquals("Test nazvu.", "blok 5", object1.introduce());

        block4.rename("blok 4");
        Assert.assertEquals("Test nazvu.", "blok 4", block4.introduce());

        object1.rename("objekt 1");
        Assert.assertEquals("Test nazvu.", "objekt 1", object1.introduce());

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 4, block2.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 3, block2.getOutputsNumber());

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 2, block3.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 2, block3.getOutputsNumber());

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 0, block1.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 0, block4.getOutputsNumber());
    }

    /**
     * Pridavani a odebirani portu.
     */
    @Test
    public void test02()
    {
        block2.delInput(2);
        Assert.assertEquals(
            "Test poctu vstupnich portu.", 3, block2.getInputsNumber());

        block2.addOutput();
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 4, block2.getOutputsNumber());

        for(int i = 0; i < 11; ++i)
            block1.addInput();
        for(int i = 0; i < 7; ++i)
            block1.addOutput();

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 11, block1.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 7, block1.getOutputsNumber());

        InPort ip1 = block1.getInput(8);
        InPort ip2 = block1.getInput(9);
        InPort ip3 = block1.getInput(10);

        OutPort op1 = block1.getOutput(0);
        OutPort op2 = block1.getOutput(1);
        OutPort op3 = block1.getOutput(2);

        for(int i = 0; i < 8; ++i)
            block1.delInput(0);
        for(int i = 0; i < 7; ++i)
            block1.delOutput(0);

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 3, block1.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 0, block1.getOutputsNumber());

        Assert.assertTrue("Mazani", block1.delInput(ip1));
        Assert.assertTrue("Mazani", block1.delInput(ip2));
        Assert.assertTrue("Mazani", block1.delInput(ip3));

        Assert.assertFalse("Jiz smazano", block1.delInput(ip1));
        Assert.assertFalse("Jiz smazano", block1.delInput(ip2));
        Assert.assertFalse("Jiz smazano", block1.delInput(ip3));

        Assert.assertFalse("Jiz smazano", block1.delOutput(op1));
        Assert.assertFalse("Jiz smazano", block1.delOutput(op2));
        Assert.assertFalse("Jiz smazano", block1.delOutput(op3));

        Assert.assertEquals(
            "Test poctu vstupnich portu.", 0, block1.getInputsNumber());
        Assert.assertEquals(
            "Test poctu vystupnich portu.", 0, block1.getOutputsNumber());
    }

    /**
     * Kontrola jmen portu.
     */
    @Test
    public void test03()
    {
        for(int i = 0; i < 5; ++i)
            block4.addOutput();

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 5, block4.getOutputsNumber());

        block4.addOutput("last");

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 6, block4.getOutputsNumber());

        Assert.assertEquals(
            "Test nazvu.",
            "last",
            block4.getOutput(block4.getOutputsNumber() - 1).introduce());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 4",
            block4.getOutput(block4.getOutputsNumber() - 2).introduce());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 0",
            block4.getOutput(0).introduce());

        block4.delOutput(0);

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 5, block4.getOutputsNumber());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 1",
            block4.getOutput(0).introduce());


        for(int i = 0; i < 3; ++i)
            block3.addOutput();

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 5, block3.getOutputsNumber());

        OutPort op = block3.addOutput("last");

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 6, block3.getOutputsNumber());

        Assert.assertEquals(
            "Test nazvu.",
            "last",
            block3.getOutput(block3.getOutputsNumber() - 1).introduce());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 4",
            block3.getOutput(block3.getOutputsNumber() - 2).introduce());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 0",
            block3.getOutput(0).introduce());

        block3.delOutput(0);

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 5, block3.getOutputsNumber());

        Assert.assertEquals(
            "Test nazvu.",
            "Výstup 1",
            block3.getOutput(0).introduce());

        block3.delOutput(op);

        Assert.assertEquals(
            "Test poctu vystupnich portu.", 4, block3.getOutputsNumber());

        Assert.assertNotEquals(
            "Test nazvu.",
            "last",
            block3.getOutput(block3.getOutputsNumber() - 1).introduce());
    }
}
