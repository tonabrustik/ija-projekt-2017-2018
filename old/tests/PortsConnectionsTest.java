
package tests;

import components.*;
import java.lang.reflect.Modifier;
import java.util.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Stanislav Bartoš
 */
public class PortsConnectionsTest {

    private InPort inPort;
    private OutPort outPort;
    private OutPort secondOutPort;

    @Before
    public void setUp() {
        Map<String, Double> type1 = new HashMap<String, Double>();
        Map<String, Double> type2 = new HashMap<String, Double>();

        type1.put("prvni", 1.2);
        type1.put("druhy", 2.3);

        type2.put("prvni", 1.2);

        inPort = new InPort(type1);
        outPort = new OutPort(type1);
        secondOutPort = new OutPort(type2);
    }

    @Test
    public void testType() {
        Map<String, Double> type = inPort.getCurrentValue();
        Assert.assertEquals("Test prvni hodnoty.", new Double(1.2), type.get("prvni"));
        Assert.assertEquals("Test druhe hodnoty.", new Double(2.3), type.get("druhy"));
        type.put("prvni", 0.5);
        inPort.setCurrentValue(type);
        Assert.assertEquals("Test hodnoty po zmene", new Double(0.5), type.get("prvni"));
        Assert.assertTrue("Test stejnych typu", inPort.hasSameType(outPort));
        Assert.assertFalse("Test ruznych typu", inPort.hasSameType(secondOutPort));
    }

    @Test
    public void testConnecting() {
        Connection connection = new Connection(inPort, outPort);
        inPort.connect(connection);
        outPort.connect(connection);

        Assert.assertEquals("Test pripojeni na vystupu", connection, outPort.getConnection());
        Assert.assertEquals("Test pripojeni na vstupu", connection, inPort.getConnection());
        Assert.assertEquals("Test vystupniho portu", outPort, connection.from());
        Assert.assertEquals("Test vstupniho portu", inPort, connection.to());

        inPort.disconnect();
        Assert.assertNull("Test odpojeni", inPort.getConnection());
    }

    @Test
    public void testWrongConnecting() {
        Connection connection = new Connection(inPort, secondOutPort);

        Assert.assertNull("Test vystupniho portu", connection.from());
        Assert.assertNull("Test vstupniho portu", connection.to());
    }

    @Test
    public void testTransmission() {
        Connection connection = new Connection(inPort, outPort);
        inPort.connect(connection);
        outPort.connect(connection);
        Map<String, Double> type = outPort.getCurrentValue();
        type.put("prvni", 0.5);
        outPort.setCurrentValue(type);
        outPort.getConnection().transferValue();
        Assert.assertEquals("Test prenosu hodnoty", new Double(0.5), inPort.getCurrentValue().get("prvni"));
    }
}
