<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Schema>
    <Blocks>
        <Block id="0">
            <name>blok 1</name>
            <x>35.0</x>
            <y>345.0</y>
            <InPorts/>
            <OutPorts>
                <Port id="0">
                    <name>Výstup 1</name>
                </Port>
                <Port id="1">
                    <name>Výstup 2</name>
                </Port>
            </OutPorts>
            <Equations>
                <Equation>z=5</Equation>
            </Equations>
        </Block>
        <Block id="1">
            <name>blok 2</name>
            <x>496.0</x>
            <y>459.0</y>
            <InPorts>
                <Port id="0">
                    <name>Vstup 1</name>
                </Port>
                <Port id="1">
                    <name>Vstup 2</name>
                </Port>
            </InPorts>
            <OutPorts>
                <Port id="0">
                    <name>Výstup 1</name>
                </Port>
            </OutPorts>
            <Equations>
                <Equation>y=z*8-i</Equation>
            </Equations>
        </Block>
        <Block id="2">
            <name>blok 3</name>
            <x>284.0</x>
            <y>113.0</y>
            <InPorts>
                <Port id="0">
                    <name>Vstup 1</name>
                </Port>
                <Port id="1">
                    <name>Vstup 2</name>
                </Port>
            </InPorts>
            <OutPorts>
                <Port id="0">
                    <name>Výstup 1</name>
                </Port>
                <Port id="1">
                    <name>Výstup 2</name>
                </Port>
            </OutPorts>
            <Equations>
                <Equation>i=z</Equation>
            </Equations>
        </Block>
        <Block id="3">
            <name>blok 4</name>
            <x>752.0</x>
            <y>152.0</y>
            <InPorts>
                <Port id="0">
                    <name>Vstup 1</name>
                </Port>
                <Port id="1">
                    <name>Vstup 2</name>
                </Port>
            </InPorts>
            <OutPorts>
                <Port id="0">
                    <name>Výstup 1</name>
                </Port>
                <Port id="1">
                    <name>Výstup 2</name>
                </Port>
            </OutPorts>
            <Equations>
                <Equation>t=y-9</Equation>
            </Equations>
        </Block>
    </Blocks>
    <Connections>
        <Connection id="0">
            <name>spojení blok 1 -&gt; blok 2</name>
            <index>1</index>
            <sourceblock>0</sourceblock>
            <sourceport>1</sourceport>
            <targetblock>1</targetblock>
            <targetport>1</targetport>
        </Connection>
        <Connection id="1">
            <name>spojení blok 1 -&gt; blok 3</name>
            <index>5</index>
            <sourceblock>0</sourceblock>
            <sourceport>0</sourceport>
            <targetblock>2</targetblock>
            <targetport>0</targetport>
        </Connection>
        <Connection id="2">
            <name>spojení blok 3 -&gt; blok 2</name>
            <index>6</index>
            <sourceblock>2</sourceblock>
            <sourceport>1</sourceport>
            <targetblock>1</targetblock>
            <targetport>0</targetport>
        </Connection>
        <Connection id="3">
            <name>spojení blok 2 -&gt; blok 4</name>
            <index>7</index>
            <sourceblock>1</sourceblock>
            <sourceport>0</sourceport>
            <targetblock>3</targetblock>
            <targetport>1</targetport>
        </Connection>
    </Connections>
</Schema>
